import Toasted from 'vue-toasted';
import Vue from "vue";
Vue.use(Toasted)


const toastOptions = {
  duration: 2000,
  keepOnHover: true,
  containerClass: "toast-custom"

}

const error = (message) => {
  Vue.toasted.error(message || "Unknown Error.", toastOptions);
};

const success = (message) => {
  Vue.toasted.success(message || "Unknown Error.", toastOptions);
};


const toast = {
  error,
  success
};

export default toast;
