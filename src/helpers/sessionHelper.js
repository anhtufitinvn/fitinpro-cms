export default {
  getItem: (name) => {
    if (typeof window === 'object' && window.localStorage) {
      return JSON.parse(localStorage.getItem(name));
    } else {
      return false;
    }
  },
  setItem: (name, data) => {
    if (typeof window === 'object' && window.localStorage) {
      return localStorage.setItem(name, JSON.stringify(data));
    } else {
      return false;
    }
  },
  removeItem: (name) => {
    if (typeof window === 'object' && window.localStorage) {
      return localStorage.removeItem(name);
    } else {
      return false;
    }
  }
}
