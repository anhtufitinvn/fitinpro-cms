import _ from "lodash";
import swal from 'sweetalert';

import authService from "../helpers/authService";

export const processNameForParent = (item) => {
    return _.repeat('--', item.depth) +  ' '  +item.name;
};

export const showSwalError = (err) => {
    swal("Error!", err.message, "error");
}

export const authorization = (roles) => {
    let user_roles = authService.getRoles();
    
    if(!roles){
        return true;
    }
    if(user_roles.some(item => roles.includes(item)) ){
        return true;
      }
    return false;
}