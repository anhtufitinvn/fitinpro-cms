import axios from 'axios';
import { get } from 'lodash';
import { ApiBase, URLLogin } from '../config';
import authService from './authService';
import router from '../router';
import Toasted from 'vue-toasted';
import Vue from "vue";
import store from "../store/index";

Vue.use(Toasted)

export default function apiBinaryPost( url, data, config, baseUrl = ApiBase) {
  console.log('config', config);
  const options = {
    baseURL: baseUrl,
    method: "POST",
    url,
    data,
    ...config,
  };
  const auth = authService.getAuth();
  if (auth) {
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${auth.data.access_token}`,
      'Content-Type': 'multipart/form-data'
    };
  }
  store.dispatch('activeSavingState');
  console.log('options', options);
  return axios(options).then(response => {
    // based on response format of fitin-api
    store.dispatch('resetSavingState');
    if (response.data.code === -401) {
      authService.setAuth(false);
      
      router.push(URLLogin);
    }

  

    if (response.data.code === -1) {
      let toastOptions = {
        duration: 2000,
        keepOnHover: true,
        containerClass: "toast-custom"

      }
      Vue.toasted.error(response.data.message || "Unknown Error. Please check your API", toastOptions);
  
      return Promise.reject(response.data);
    }

    return {
      data: response.data
    };
  })
  .catch(err => {
    console.log('err',err);
    store.dispatch('resetSavingState');
    let status = get(err,'response.code',false);
    if (status === -401) {
      authService.setAuth(false);
      router.push(URLLogin);
    }
    return Promise.reject(err);
  }) ;
}