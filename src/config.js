export const ssn = {
    user: "userLocal",
    auth: "authLocal",
    permissions: "permissionsLocal",
    roles : "rolesLocal",
    profile: "userProfileLocal"
  };

  
  //************************************ URL API ************************************ //
  

  export const ApiBase = (process.env.VUE_APP_API_3D_URL || "") + "/editor/cms/v2/";

  export const ApiConsultantProject = (process.env.VUE_APP_API_CONSULTANT_URL || "");

  export const ApiDashboard = {
    overall : ApiBase + "dashboard/overall",
    newestUsers : ApiBase + "dashboard/newest-users",
    newestFeedbacks : ApiBase + "dashboard/newest-feedbacks"
  };

  export const ApiLogin = ApiBase + "auth/login";
  export const ApiChangePassword = ApiBase + "auth/password";
  export const ApiProfile = ApiBase + "auth/profile";
  export const ApiUploadAvatar = ApiBase + "auth/avatar";
  export const ApiOrders = ApiBase + "orders";
  export const ApiTickets = ApiBase + "tickets";
  
  export const ApiSearch = ApiBase + "search";
  export const ApiTicketsRequest = ApiBase + "tickets/request/order";
  export const ApiTicketsProgress = ApiBase + "tickets/ticketId/progress";

  
  //Object
  export const ApiObjects = ApiBase + "objects";
  export const ApiUploadObjectAssetBundle = ApiBase + 'upload/objects/asset-bundle';
  export const ApiUploadObjectThumb = ApiBase + 'upload/objects/thumb';

  //Users
  export const ApiUsers = ApiBase + "users";

  //CMS Users
  export const ApiCMSUsers = ApiBase + "cms-users";

  export const ApiRoles = ApiBase + "roles";

  export const ApiPermissions = ApiBase + "permissions";

  //Bundles
  export const ApiBundles = ApiBase + "bundles";
  export const ApiUploadBundleThumb = ApiBase + 'upload/bundles/thumb';
  export const ApiApproveBundle = ApiBase + 'bundles/:key/approved';
  export const ApiRejectBundle = ApiBase + 'bundles/:key/rejected';
  export const ApiGetListBundleItems = ApiBase + 'bundles/:key/list-items';

  //Rooms
  export const ApiRooms = ApiBase + "rooms";
  export const ApiUploadRoomAssetBundle = ApiBase + 'upload/rooms/asset-bundle';
  export const ApiUploadRoomThumb = ApiBase + 'upload/rooms/thumb';
  export const ApiApproveRoom = ApiBase + 'rooms/:key/approved';
  export const ApiRejectRoom = ApiBase + 'rooms/:key/rejected';
  export const ApiGetListRoomItems = ApiBase + 'rooms/:key/list-items';

  //Layouts
  export const ApiLayouts = ApiBase + "layouts";
  export const ApiLayoutGallery = ApiBase + "layouts/:key/gallery";
  export const ApiUploadLayoutAssetBundle = ApiBase + 'upload/layouts/asset-bundle';
  export const ApiUploadLayoutThumb = ApiBase + 'upload/layouts/thumb';
  export const ApiUploadLayoutImageFloorPlan = ApiBase + 'upload/layouts/floor-plan';
  export const ApiUploadLayoutPanoramaZip = ApiBase + 'upload/layouts/panorama-zip';
  export const ApiUploadLayoutGallery = ApiBase + 'upload/layouts/gallery';
  export const ApiApproveLayout = ApiBase + 'layouts/:key/approved';
  export const ApiRejectLayout = ApiBase + 'layouts/:key/rejected';
  export const ApiGetListLayoutItems = ApiBase + 'layouts/:key/list-items';
  export const ApiGetLayoutDataJson = ApiBase + 'layouts/:key/data-json';
  export const ApiSyncLayoutFromEcom = ApiBase + 'layouts/sync/ecom';
  //Brands
  export const ApiBrands = ApiBase + "brands";
  export const ApiUploadBrandThumb = ApiBase + 'upload/brands/thumb';

  //Categories
  export const ApiCategories = ApiBase + "categories";
  export const ApiUploadCategoryThumb = ApiBase + 'upload/categories/thumb';

  //RoomCategories
  export const ApiRoomCategories = ApiBase + "room-categories";
  export const ApiUploadRoomCategoryThumb = ApiBase + 'upload/room-categories/thumb';

  //Project
  export const ApiProjects = ApiBase + "projects";
  export const ApiUploadProjectThumb = ApiBase + 'upload/projects/thumb';
  
  //Style
  export const ApiStyles = ApiBase + "styles";

  //Pages
  export const ApiPages = ApiBase + "pages";

  //Feedback
  export const ApiFeedbacks = ApiBase + "feedbacks";

  //Materials
  export const ApiMaterials = ApiBase + "materials";
  export const ApiUploadMaterialAssetBundle = ApiBase + 'upload/materials/asset-bundle';
  export const ApiUploadMaterialThumb = ApiBase + 'upload/materials/thumb';
  export const ApiUploadMaterialByExcel = ApiBase + 'materials/excel';
  
  //MaterialCategories
  export const ApiMaterialCategories = ApiBase + "material-categories";
  export const ApiUploadMaterialCategoryThumb = ApiBase + 'upload/material-categories/thumb';

  //MaterialBrands
  export const ApiMaterialBrands = ApiBase + "material-brands";
  export const ApiUploadMaterialBrandThumb = ApiBase + 'upload/material-brands/thumb';

  //ColorBrands
  export const ApiColorBrands = ApiBase + "color-brands";
  export const ApiUploadColorBrandThumb = ApiBase + 'upload/color-brands/thumb';

  //ColorGroups
  export const ApiColorGroups = ApiBase + "color-groups";

  //Colors
  export const ApiColors = ApiBase + "colors";
  export const ApiUploadColorByExcel = ApiBase + 'colors/excel';
  //Version Build
  export const ApiVersions = ApiBase + "config/build/all";
  export const ApiUploadVersion = ApiBase + "config/upload/build";
  export const ApiEditVersion = ApiBase + "config/build";

  //Location
  export const ApiLocations = ApiBase + "locations";

  //************************************ URL WEB ************************************ //
  export const URLPublic = process.env.VUE_APP_PUBLIC_URL || "/";
  export const URLLogin = URLPublic + "auth/login";
  export const URLProfile = URLPublic + "auth/profile";
  export const URLOrders = URLPublic + "orders";
  export const URLHome = URLPublic + "home";
  
  export const URLSearch = URLPublic + "search";
  export const URLTickets = URLPublic + "tickets";
  export const URLTicketsProgress = URLPublic + "tickets/progress";

  export const URLObjects = URLPublic + "objects";
  export const URLObjectTTS = URLPublic + "object-tts";
  export const URLObjectItems = URLPublic + "object-items";
  export const URLUsers = URLPublic + "users";
  export const URLCMSUsers = URLPublic + "cms-users";

  export const URLRoles = URLPublic + "roles";
  export const URLPermissions = URLPublic + "permissions";

  export const URLBundles = URLPublic + "bundles";
  export const URLRooms = URLPublic + "rooms";
  export const URLLayouts = URLPublic + "layouts";
  export const URLBrands = URLPublic + "brands";
  export const URLMaterialBrands = URLPublic + "material-brands";
  export const URLCategories = URLPublic + "categories";
  export const URLRoomCategories = URLPublic + "room-categories";
  export const URLMaterialCategories = URLPublic + "material-categories";
  export const URLProjects = URLPublic + "projects";
  export const URLPages = URLPublic + "pages";

  export const URLVersionBuild = URLPublic + "build-version";
  export const URLFeedbacks = URLPublic + "feedbacks";

  export const URLMaterials = URLPublic + "materials";

  export const URLColorBrands = URLPublic + "color-brands";
  export const URLColorGroups = URLPublic + "color-groups";
  export const URLColors = URLPublic + "colors";
    //************************************ OTHER CONSTANT ************************************ //

  export const ConsultantProjectAccessKey = "JuggAmTaPaTechies45MinGG";

  export const STATUS_LIST = {
    published : "Published",
    pending : "Pending"
  };

  export const LAYOUT_STATUS_LIST = {
    published : "Published",
    pending : "Pending",
    waiting : "Waiting",
    private : "Private",
    rejected : "Rejected"
  };


  export const MATERIAL_TYPES = {
    wall : "Wall",
    floor : "Floor",
    ceiling : "Ceiling"
  };


  export const ROLES = {
    Page : ["admin"],
    Object : ["admin", "designer"],
    Project : ["admin", "designer"],
    Bundle : ["admin", "designer"],
    Room : ["admin", "designer"],
    Layout : ["admin", "designer"],
    Category : ["admin", "designer"],
    Material : ["admin", "designer"],
    MaterialCategory : ["admin", "designer"],
    RoomCategory : ["admin", "designer"],
    Brand : ["admin", "designer"],
    MaterialBrand : ["admin", "designer"],
    Color : ["admin", "designer"],
    ColorBrand : ["admin", "designer"],
    ColorGroup : ["admin", "designer"],
    Version : ["admin"],
    Feedback : ["admin"],
    User : ["admin"],
    CMSUser : ["admin"],
    Role: ["admin"],
    Permission: ["admin"]
   
  }
