/**
 * Store to handle app theme style
 */
import Vue from 'vue'
import { ApiOrders } from "../../config"
import apiCall from '../../helpers/apiCall'

const OrderModule = {
  state: {
    orderData: {}
  },
  getters: {
   getOrderData(state){
    return state.orderData;
   }
  },
  mutations: {
    SET_ORDER_DATA(state, data){
        state.orderData = data;
    },
  },
  actions: {
    getOrderById({ commit }, id){
        if (id) {
            apiCall('get', ApiOrders + "/" + id).then(res => {
              if (res.data.code == 0) {
                    commit('SET_ORDER_DATA', res.data.data)
                }
            });
        }
    },
  }
};

export default OrderModule;
