import { ApiTickets, ApiTicketsRequest, ApiTicketsProgress } from "../../config"
import apiCall from '../../helpers/apiCall'
import Vue from 'vue'

const SearchModule = {
  state: {
    ticketData: [],
    ticketDataById: {}
  },

  getters: {
    getTicketData(state){
        return state.ticketData;
    },
    getTicketDataById(state){
        return state.ticketDataById;
    },
    postTicketRequest(state){
        return state.ticketRequest;
    }
  },

  mutations: {
    SET_TICKET_DATA(state, data){
        state.ticketData = data;
    },
    SET_TICKET_DATA_BY_ID(state, data){
        state.ticketDataById = data;
    },
    SET_TICKET_REQUEST_DATA(state, data){
        state.ticketRequest = data;
    }
  },

  actions: {
    getTicket({ commit }){
        return new Promise(function(resolve, reject) {
            try {
                apiCall('get', ApiTickets).then(res => {
                    if (!res.data.error) {
                        commit('SET_TICKET_DATA', res.data.data.list)
                         resolve(res.data.data.list);
                    }
                })
            } catch (error) {
                Vue.toasted.show(error, {className: 'bg-danger text-white',duration: '2000'});
                reject({ status: false });
                }
          });
       
    },

    getTicketById({ commit }, ticketId){
        if (ticketId) {
            apiCall('get', ApiTickets + "/" + ticketId).then(res => {
                if (!res.data.error) {
                commit('SET_TICKET_DATA_BY_ID', res.data.data)
                }
            });
        }
    },

    updateTicket({ commit }, payload) {
        console.log('payload.ticketId',payload.ticketId);
        apiCall("put", ApiTickets + "/" + payload.ticketId, payload)
        .then(res => {
            if (res.data.code && res.data.code === 0 && res.data.message && res.data.message === "Success") {
                commit('SET_TICKET_DATA_BY_ID', res.data.data)
                Vue.toasted.show('sssssss' + res.data.message, {className: 'bg-success text-white',duration: '2222222'});
            }
            else {
                Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '222222'});
            
            }
        })
        .catch(error => {
            console.error(error);
        });
    },

    postTicket({ commit }, payload) {
        apiCall("post", ApiTickets, payload)
        .then(res => {
            if (res.data.code && res.data.code === 0 && res.data.message && res.data.message === "Success") {
                commit('SET_TICKET_DATA', res.data.data)
                Vue.toasted.show(res.data.message, {className: 'bg-success text-white',duration: '2000'});
            }
            else {
                Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '2000'});
            
            }
        })
        .catch(error => {
            console.error(error);
        });
    },

    postTicketRequest({ commit }, payload) {
        apiCall("post", ApiTicketsRequest, payload)
        .then(res => {
            if (res.data.code && res.data.code === 0 && res.data.message && res.data.message === "Success") {
                commit('SET_TICKET_REQUEST_DATA', res.data.data)
                Vue.toasted.show(res.data.message, {className: 'bg-success text-white',duration: '2000'});
            }
            else {
                Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '2000'});
            
            }
        })
        .catch(error => {
            console.error(error);
        });
      },

    postTicketProgress({ commit }, payload) {
        apiCall("post", ApiTicketsProgress.replace('ticketId', payload.ticketId) , payload)
        .then(res => {
            if (res.data.code == 0) {
                commit('SET_TICKET_REQUEST_DATA', res.data.data)
                Vue.toasted.show(res.data.message, {className: 'bg-success text-white',duration: '2000'});
            }
            else {
                Vue.toasted.show(res.data.message,{className: 'bg-danger text-white',duration: '2000'});
            
            }
        })
        .catch(error => {
            console.error(error);
        });
    }
    
  }
};

export default SearchModule;
