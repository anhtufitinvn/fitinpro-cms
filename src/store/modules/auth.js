/**
 * Store to handle app theme style
 */
import apiCall from "../../helpers/apiCall";
import { ApiLogin } from "../../config";
import { get, isEmpty } from "lodash";
import authService from "../../helpers/authService";
const authLocal = authService.getAuth();
const AuthModule = {
  state: {
    auth: authLocal || null,
    token: get(authLocal, 'access_token', ''),
    token3D: get(authLocal, 'access_token_fitin3d', ''),
    permissions: get(authLocal, 'permissions', []),
    roles: get(authLocal, 'roles', []),
    profile :  get(authLocal, 'profile', []),
  },
  getters: {
    isLogin(state) {
      return isEmpty(state.auth);
    },
    getToken(state) {
      return state.token;
    },
    // getToken3D(state) {
    //   return state.token3D;
    // },
    // getPermissions(state) {
    //   return state.permissions;
    // },
    getRoles(state){
      return state.roles;
    },
    getProfile(state){
      return authService.getProfile();
    }
  },
  mutations: {
    /**
     * Change State without Async
     */
    login(state, data) {
      state.auth = data;
      state.token = data.data.access_token;
      state.profile = data.data.auth;
      authService.setAuth(data);
      
      authService.setProfile(data.data.auth);
    },
    // permissions(state, data) {
    //   state.permissions = data.data.permissions;
    //   state.role = data.data.role;
    //   const authLocal = authService.getAuth();
    //   authLocal.permissions = state.permissions;
    //   authLocal.role = state.role;
    //   authService.setAuth(authLocal);
    // },
    logout(state) {
      state.auth = null;
      state.token = '';
      state.permissions = [];
      authService.setAuth(false);
      authService.setProfile(null);
      authService.setRoles([]);
    },
    // profile(state, data){
    //   state.profile = data.data.data.auth;
    //   const authLocal = authService.getAuth();
    //   authLocal.profile = state.data;
    //   authService.setAuth(authLocal);
    // },
    roles(state, data){
      state.roles = data.data.auth.roles;
      //const roleLocal = authService.getRoles();
      let roles = state.roles || [];         
      roles = roles.map(function(item) { return item["name"]; });
      //roleLocal.roles = state.roles;
      authService.setRoles(roles);
    }
  },
  actions: {
    /**
     * Change State without Async
     */
    actionLogin({ commit }, data) {
      return apiCall("post", ApiLogin, data)
        .then(data => {
          
          if (get(data, 'data.data.access_token')) {
            commit("login", data.data);
            commit("roles", data.data);
            return Promise.resolve(data);
          } else {
            const message = get(data, "data.message", "");
            return Promise.reject(message);
          }
        })
        .catch(error => {
          const message = get(error, "message", "");
          return Promise.reject(message);
        });
    },
    actionLogout({ commit }) {
      return new Promise(function(resolve, reject) {
        try {
          commit("logout");
          resolve({ status: true });
        } catch (error) {
          console.log(error);
          reject({ status: false });
        }
      });
    }
  }
};

export default AuthModule;
