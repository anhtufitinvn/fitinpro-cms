
import {
    URLHome, 
    URLObjects, URLObjectTTS, URLObjectItems,
    URLUsers, 
    URLCMSUsers,
    URLRoles,
    URLPermissions,
    URLBundles,
    URLRooms,
    URLLayouts,
    URLBrands,
    URLMaterialBrands,
    URLCategories,
    URLRoomCategories,
    URLMaterialCategories,
    URLProjects,
    URLPages,
    URLVersionBuild,
    URLFeedbacks,
    URLMaterials,

    URLColorBrands,
    URLColorGroups,
    URLColors,

} from "./config";

//ROle Group from config.ROLES
const Menu = [
    {
        name: 'Home',
        icon: 'fa fa-th-large',
       // translate: 'sidebar.nav.DASHBOARD',
        path: URLHome
    },
    {
        name: 'Users',
        icon: 'fas fa-user-tie',
        //path: URLUsers,
        submenu: [
            {
                name: 'Editor',
                path: URLUsers,
                roleGroup: 'User'
            },
            {
                name: 'CMS Accounts',
                path: URLCMSUsers,
                roleGroup: 'CMSUser'
            },
            {
                name: 'CMS Role',
                path: URLRoles,
                roleGroup: 'Role'
            },
            {
                name: 'CMS Permission',
                path: URLPermissions,
                roleGroup: 'Permission'
            }
        ]
       // translate: 'sidebar.nav.PROFILE'
    },
    // {
    //     name: 'Tickets',
    //     icon: 'icon-puzzle',
    //     path: 'https://docs.google.com/spreadsheets/d/1NlFsRjen27W3idk3jLc-IR4JaD0dJQ0vPmuBfl8ngUE/edit#gid=0',
    //     outsite: true,
    //     // submenu: [
    //     //     {
    //     //         name: 'Tickets Management',
    //     //         path: URLTickets
    //     //     },
    //     //     {
    //     //         name: 'Tickets Progress',
    //     //         path: URLTicketsProgress
    //     //     }
            
    //     // ]
    // },
    {
        name: 'Objects',
        icon: 'fa fa-cube',
        outsite: false,
        //path: URLObjects,
        //translate: 'sidebar.nav.ORDERS',
        roleGroup: 'Object',
        submenu: [
            {
                name: 'Items',
                path: URLObjectItems
            },
            {
                name: 'TTS',
                path: URLObjectTTS
            }
            
        ]
    },
    {
        name: 'Bundles',
        icon: 'fa fa-cubes',
        path: URLBundles,
        roleGroup: 'Bundle',
      
    },
    {
        name: 'Rooms',
        icon: 'fas fa-inbox',
        path: URLRooms,
        roleGroup: 'Room',
      
    },

    {
        name: 'Layouts',
        icon: 'fas fa-building',
        path: URLLayouts,
        roleGroup: 'Layout',
      
    },

    {
        name: 'Brands',
        icon: 'fas fa-project-diagram',
        submenu: [
            {
                name: 'Object Brands',
                path: URLBrands,
                roleGroup: 'Brand'
            },
            {
                name: 'Material Brands',
                path: URLMaterialBrands,
                roleGroup: 'MaterialBrand'
            },
            {
                name: 'Color Brands',
                path: URLColorBrands,
                roleGroup: 'ColorBrand'
            }
        ]
    },

    {
        name: 'Categories',
        icon: 'fa fa-puzzle-piece',
        outsite: false,
        submenu: [
            {
                name: 'Object Categories',
                path: URLCategories,
                roleGroup: 'Category'
            },
            {
                name: 'Room Categories',
                path: URLRoomCategories,
                roleGroup: 'RoomCategory'
            },

            {
                name: 'Material Categories',
                path: URLMaterialCategories,
                roleGroup: 'MaterialCategory'
            }
        ]
    },


    {
        name: 'Projects',
        icon: 'fa fa-folder-open',
        path: URLProjects,
        roleGroup: 'Project'     
    },

    {
        name: 'Materials',
        icon: 'fa fa-paint-brush',
        path: URLMaterials,
        roleGroup: 'Material'    
    },

    {
        name: 'Colors',
        icon: 'fa fa-palette',
        outsite: false,
        submenu: [
            {
                name: 'Color Groups',
                path: URLColorGroups,
                roleGroup: 'ColorGroup'
            },
            {
                name: 'Colors',
                path: URLColors,
                roleGroup: 'Color'
            }
        ]
    },

    {
        name: 'Settings',
        icon: 'fa fa-cog',
        outsite: false,
        submenu: [
            {
                name: 'Page',
                //icon: 'fa fa-folder-open',
                path: URLPages,
                roleGroup: 'Page'
            },
            {
                name: 'Build Version Panel',
                path: URLVersionBuild,
                roleGroup: 'Version'
            },

            {
                name: 'User Feed Back',
                path: URLFeedbacks,
                roleGroup: 'Feedback'
            }
        ]
    },
   
];

export default Menu;