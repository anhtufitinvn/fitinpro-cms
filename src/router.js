import Vue from 'vue'
import Router from 'vue-router'

import authService from "./helpers/authService";

const Login = () => import("@/views/Auth/Login.vue");
const Profile = () => import("@/views/Auth/Profile.vue");
// Search MAIN
const SearchPage = () => import('@/views/Search/SearchPage.vue')

// Profile
const Users = () => import('@/views/Users/ListView.vue')

// CMS Account
const CMSUsers = () => import('@/views/CMSUsers/ListView.vue')

// Roles
const Roles = () => import('@/views/Roles/ListView.vue')
const Permissions = () => import('@/views/Permissions/ListView.vue')

// Dashboard MAIN
const Home = () => import('@/views/Home/Home.vue')
const Objects = () => import('@/views/Objects/ListView.vue')

// Bundles
const Bundles = () => import('@/views/Bundles/ListView.vue')

// Rooms
const Rooms = () => import('@/views/Rooms/ListView.vue')

// Layouts
const Layouts = () => import('@/views/Layouts/ListView.vue')

//Brands
const Brands = () => import('@/views/Brands/ListView.vue')

//Categories
const Categories = () => import('@/views/Categories/ListView.vue')

//RoomCategories
const RoomCategories = () => import('@/views/RoomCategories/ListView.vue')

//MaterialCategories
const MaterialCategories = () => import('@/views/MaterialCategories/ListView.vue')

//MaterialBrands
const MaterialBrands = () => import('@/views/MaterialBrands/ListView.vue')


//ColorBrands
const ColorBrands = () => import('@/views/ColorBrands/ListView.vue')

//ColorGroups
const ColorGroups = () => import('@/views/ColorGroups/ListView.vue')


//Colors
const Colors = () => import('@/views/Colors/ListView.vue')

//Projects
const Projects = () => import('@/views/Projects/ListView.vue')

//Pages
const Pages = () => import('@/views/Pages/ListView.vue')

//Materials
const Materials = () => import('@/views/Materials/ListView.vue')

//Feedbacks
const Feedbacks = () => import('@/views/Feedbacks/ListView.vue')

//Build Version
const BuildVersion = () => import('@/views/Version/ListView.vue')

// Layouts
import Layout from '@/components/Layout/Layout'
// import LayoutHorizontal from '@/components/Layout/LayoutHorizontal'
// import LayoutPage from '@/components/Layout/LayoutPage'
import Empty from "@/components/Layout/Empty";

Vue.use(Router)
import {
  URLPublic, URLLogin, URLProfile,
  URLHome, URLOrders, 
  URLUsers, 
  URLCMSUsers, 
  URLRoles, 
  URLPermissions,
  URLSearch, URLTickets, URLTicketsProgress, 
  URLObjects, URLObjectTTS, URLObjectItems, 
  URLBundles,
  URLRooms,
  URLLayouts,
  URLBrands,
  URLMaterialBrands,
  URLCategories,
  URLRoomCategories,
  URLMaterialCategories,
  URLProjects,
  URLPages,
  URLVersionBuild,
  URLFeedbacks,
  URLMaterials,

  URLColorBrands,
  URLColorGroups,
  URLColors,

  ROLES
} from "./config";


const router = new Router({
    mode: "history",
    base: URLPublic,
    routes: [
      {
        path: "/",
        redirect: URLHome
      },
      {
        path: "/",
        component: Layout,
        beforeEnter: (to, from, next) => {
          // ...
          if (authService.isAuthenticated()) {
            next();
          } else {
            next({ path: URLLogin });
          }
        },
        children: [
            //  Home
            {
                path: URLHome,
                component: Home
                
            },
            {
              path: URLProfile,
              component: Profile
              
            },
            {
              component: SearchPage,
              path: URLSearch,
              meta : {
                roles : ROLES.Page
              }
            },
            // Users
            {
                component: Users,
                path: URLUsers,
                meta : {
                  roles : ROLES.User
                }
            },

            {
              component: CMSUsers,
              path: URLCMSUsers,
              meta : {
                roles : ROLES.CMSUser
              }
            },

            {
              component: Roles,
              path: URLRoles,
              meta : {
                roles : ROLES.Role
              }
            },
            {
              component: Permissions,
              path: URLPermissions,
              meta : {
                roles : ROLES.Permission
              }
            },
            // {
            //     component: ProfileDetail,
            //     path: URLProfile + "/:id",
            // },
            // Tickets Management
            // {
            //     component: TicketProgress,
            //     path: URLTicketsProgress,
            // },
            // {
            //   component: TicketsManagement,
            //   path: URLTickets,
            // },
            // {
            //   component: DetailTickets,
            //   path: URLTickets + "/:id",
            // },
            // {
            //   component: Orders,
            //   path: URLOrders,
            //   meta : {
            //     roles : ["admin"]
            //   }
            // },
            // {
            //   component: OrdersDetail,
            //   path: URLOrders + "/:id",
            //   meta : {
            //     roles : ["admin"]
            //   }
            // },

            //  Object
            {
              path: URLObjectTTS,
              component: Objects,
              props: (route) => ({ type: 'tts' }) ,
              meta : {
                roles : ROLES.Object
              }
            },
            {
              path: URLObjectItems,
              component: Objects,
              props: (route) => ({ type: 'item' }),
              meta : {
                roles : ROLES.Object
              }
              
            },

            // Bundles
            {
              component: Bundles,
              path: URLBundles,
              meta : {
                roles : ROLES.Bundle
              }
            },

            // Rooms
            {
              component: Rooms,
              path: URLRooms,
              meta : {
                roles : ROLES.Room
              }
            },

            // Layouts
            {
              component: Layouts,
              path: URLLayouts,
              meta : {
                roles : ROLES.Layout
              }
            },

            // Brand
            {
              component: Brands,
              path: URLBrands,
              meta : {
                roles : ROLES.Brand
              }
            },

            // Material Brand
            {
              component: MaterialBrands,
              path: URLMaterialBrands,
              meta : {
                roles : ROLES.MaterialBrand
              }
            },

            // Category
            {
              component: Categories,
              path: URLCategories,
              meta : {
                roles : ROLES.Category
              }
            },

            // RoomCategory
            {
              component: RoomCategories,
              path: URLRoomCategories,
              meta : {
                roles : ROLES.RoomCategory
              }
            },

            // MaterialCategory
            {
              component: MaterialCategories,
              path: URLMaterialCategories,
              meta : {
                roles : ROLES.MaterialCategory
              }
            },

            // Color Brand
            {
              component: ColorBrands,
              path: URLColorBrands,
              meta : {
                roles : ROLES.ColorBrand
              }
            },

            // Color Group
            {
              component: ColorGroups,
              path: URLColorGroups,
              meta : {
                roles : ROLES.ColorGroup
              }
            },

            // Color
            {
              component: Colors,
              path: URLColors,
              meta : {
                roles : ROLES.Color
              }
            },
            //Project
            {
              component: Projects,
              path: URLProjects,
              meta : {
                roles : ROLES.Project
              }
            },

            // Page
            {
              component: Pages,
              path: URLPages,
              meta : {
                roles : ROLES.Page
              }
            },

            // Material
            {
              component: Materials,
              path: URLMaterials,
              meta : {
                roles : ROLES.Material
              }
            },

            // Feedback
            {
              component: Feedbacks,
              path: URLFeedbacks,
              meta : {
                roles : ROLES.Feedback
              }
            },

            // Build Version
            {
              component: BuildVersion,
              path: URLVersionBuild,
              meta : {
                roles : ROLES.Version
              }
            }
        ]
      },
      {
        path: URLLogin,
        component: Empty,
        children: [
          {
            path: "",
            component: Login
          }
        ]
      },
      // Not found route
      {
        path: "*",
        redirect: "/"
      }
    ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const meta = to.meta;

  let roles = authService.getRoles();
  if (meta.roles) { 
    if(meta.roles.some(item => roles.includes(item)) ){
      return next();
    }
    else {
      next('/');
    }
  }else{
    next();
  } 
  
});

export default router;